Microsoft Windows [Version 10.0.16299.371]
(c) 2017 Microsoft Corporation. All rights reserved.

C:\Users\Jason>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\Users\Jason

04/19/2018  07:16 PM    <DIR>          .
04/19/2018  07:16 PM    <DIR>          ..
03/29/2018  08:26 PM    <DIR>          .config
04/19/2018  07:52 PM    <DIR>          .docker
03/29/2018  08:36 PM    <DIR>          .electron
04/04/2018  01:28 PM               238 .gitconfig
03/03/2018  12:08 PM    <DIR>          .ssh
02/22/2018  09:36 PM    <DIR>          .TI-trace
03/03/2018  01:32 PM    <DIR>          .vagrant.d
03/10/2018  12:58 PM    <DIR>          .VirtualBox
03/29/2018  06:38 PM    <DIR>          .vscode
10/22/2017  08:30 AM    <DIR>          .zenmap
04/10/2018  05:39 PM    <DIR>          3D Objects
11/11/2017  11:05 AM    <DIR>          Anaconda3
04/10/2018  05:39 PM    <DIR>          Contacts
04/10/2018  05:39 PM    <DIR>          Desktop
04/21/2018  10:41 AM    <DIR>          Documents
04/13/2018  06:41 PM    <DIR>          Downloads
10/20/2017  05:14 AM    <DIR>          Evernote
04/10/2018  05:39 PM    <DIR>          Favorites
04/19/2018  07:21 PM    <DIR>          iCloudDrive
04/10/2018  05:39 PM    <DIR>          Links
04/10/2018  05:39 PM    <DIR>          Music
04/21/2018  10:58 AM    <DIR>          OneDrive
04/21/2018  10:58 AM    <DIR>          OneDrive - Robarts Ventures
04/10/2018  05:39 PM    <DIR>          Pictures
04/10/2018  05:39 PM    <DIR>          Saved Games
04/10/2018  05:39 PM    <DIR>          Searches
03/03/2018  07:03 PM    <DIR>          ti
02/22/2018  09:24 PM    <DIR>          TICloudAgent
04/10/2018  05:39 PM    <DIR>          Videos
03/03/2018  11:45 AM    <DIR>          VirtualBox VMs
03/04/2018  07:23 AM    <DIR>          workspace_v7
03/03/2018  05:52 PM    <DIR>          workspace_v7_cc
               1 File(s)            238 bytes
              33 Dir(s)  280,352,690,176 bytes free

C:\Users\Jason>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\Users\Jason

04/19/2018  07:16 PM    <DIR>          .
04/19/2018  07:16 PM    <DIR>          ..
03/29/2018  08:26 PM    <DIR>          .config
04/19/2018  07:52 PM    <DIR>          .docker
03/29/2018  08:36 PM    <DIR>          .electron
04/04/2018  01:28 PM               238 .gitconfig
03/03/2018  12:08 PM    <DIR>          .ssh
02/22/2018  09:36 PM    <DIR>          .TI-trace
03/03/2018  01:32 PM    <DIR>          .vagrant.d
03/10/2018  12:58 PM    <DIR>          .VirtualBox
03/29/2018  06:38 PM    <DIR>          .vscode
10/22/2017  08:30 AM    <DIR>          .zenmap
04/10/2018  05:39 PM    <DIR>          3D Objects
11/11/2017  11:05 AM    <DIR>          Anaconda3
04/10/2018  05:39 PM    <DIR>          Contacts
04/10/2018  05:39 PM    <DIR>          Desktop
04/21/2018  10:41 AM    <DIR>          Documents
04/13/2018  06:41 PM    <DIR>          Downloads
10/20/2017  05:14 AM    <DIR>          Evernote
04/10/2018  05:39 PM    <DIR>          Favorites
04/19/2018  07:21 PM    <DIR>          iCloudDrive
04/10/2018  05:39 PM    <DIR>          Links
04/10/2018  05:39 PM    <DIR>          Music
04/21/2018  10:58 AM    <DIR>          OneDrive
04/21/2018  10:58 AM    <DIR>          OneDrive - Robarts Ventures
04/10/2018  05:39 PM    <DIR>          Pictures
04/10/2018  05:39 PM    <DIR>          Saved Games
04/10/2018  05:39 PM    <DIR>          Searches
03/03/2018  07:03 PM    <DIR>          ti
02/22/2018  09:24 PM    <DIR>          TICloudAgent
04/10/2018  05:39 PM    <DIR>          Videos
03/03/2018  11:45 AM    <DIR>          VirtualBox VMs
03/04/2018  07:23 AM    <DIR>          workspace_v7
03/03/2018  05:52 PM    <DIR>          workspace_v7_cc
               1 File(s)            238 bytes
              33 Dir(s)  280,352,256,000 bytes free

C:\Users\Jason>cd ..

C:\Users>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\Users

11/05/2017  04:54 PM    <DIR>          .
11/05/2017  04:54 PM    <DIR>          ..
04/19/2018  07:16 PM    <DIR>          Jason
11/05/2017  02:14 PM    <DIR>          Public
               0 File(s)              0 bytes
               4 Dir(s)  280,335,118,336 bytes free

C:\Users>cd \src

C:\src>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\src

04/13/2018  06:42 PM    <DIR>          .
04/13/2018  06:42 PM    <DIR>          ..
04/13/2018  06:42 PM    <DIR>          Artificial Intelligence A-Z
03/29/2018  08:27 PM    <DIR>          electron-quick-start
02/19/2018  05:26 PM               406 id_rsa_mybash.pub
04/19/2018  06:42 PM    <DIR>          iot-310a-sp18-code
04/19/2018  07:15 PM    <DIR>          iot-310a-sp18-slides
03/30/2018  08:14 PM    <DIR>          iot-310b-slides
03/22/2018  06:23 PM    <DIR>          iot-310b-student
12/21/2017  08:01 PM    <DIR>          iot110
12/03/2017  08:07 AM    <DIR>          iot110-student
03/05/2018  02:04 PM    <DIR>          iot210
04/21/2018  10:33 AM    <DIR>          iot310
03/03/2018  11:37 AM    <DIR>          openthread
03/01/2018  08:44 PM    <DIR>          pi-iot210-6lowpan
02/18/2018  01:49 PM    <DIR>          PiBackup
03/01/2018  08:47 PM    <DIR>          uw_iot_210_student
               1 File(s)            406 bytes
              16 Dir(s)  280,334,893,056 bytes free

C:\src>cd iot310

C:\src\iot310>cd week5
The system cannot find the path specified.

C:\src\iot310>cd wee4
The system cannot find the path specified.

C:\src\iot310>cd week4

C:\src\iot310\week4>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\src\iot310\week4

04/21/2018  10:35 AM    <DIR>          .
04/21/2018  10:35 AM    <DIR>          ..
04/21/2018  10:33 AM    <DIR>          assets
04/19/2018  06:42 PM             1,119 centralVirtualDevice.py
04/21/2018  10:36 AM               939 Dockerfile
04/19/2018  08:33 PM            36,773 README.md
04/21/2018  10:36 AM                95 test.txt
               4 File(s)         38,926 bytes
               3 Dir(s)  280,345,726,976 bytes free

C:\src\iot310\week4>code .

C:\src\iot310\week4>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\src\iot310\week4

04/21/2018  10:35 AM    <DIR>          .
04/21/2018  10:35 AM    <DIR>          ..
04/21/2018  10:33 AM    <DIR>          assets
04/19/2018  06:42 PM             1,119 centralVirtualDevice.py
04/21/2018  10:36 AM               939 Dockerfile
04/19/2018  08:33 PM            36,773 README.md
04/21/2018  10:36 AM                95 test.txt
               4 File(s)         38,926 bytes
               3 Dir(s)  280,328,617,984 bytes free

C:\src\iot310\week4>docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES

C:\src\iot310\week4>docker images
REPOSITORY                        TAG                 IMAGE ID            CREATED             SIZE
central_virtual_device            latest              5ffce9dc913e        24 minutes ago      691MB
jerobart/central_virtual_device   latest              73e99e9cadb7        38 hours ago        691MB
jerobart/central_virual_device    latest              73e99e9cadb7        38 hours ago        691MB
<none>                            <none>              7a7d19081587        39 hours ago        691MB
<none>                            <none>              700104ccc5e3        39 hours ago        702MB
ubuntu                            latest              c9d990395902        8 days ago          113MB
hello-world                       latest              e38bc07ac18e        10 days ago         1.85kB
python                            2.7                 2863c80c418c        4 weeks ago         679MB

C:\src\iot310\week4>docker run -it central_virtual_device
Traceback (most recent call last):
  File "centralVirtualDevice.py", line 21, in <module>
    mqttc.connect(brokerHost, brokerPort, brokerTimeout)
  File "/usr/local/lib/python2.7/site-packages/paho/mqtt/client.py", line 768, in connect
    return self.reconnect()
  File "/usr/local/lib/python2.7/site-packages/paho/mqtt/client.py", line 895, in reconnect
    sock = socket.create_connection((self._host, self._port), source_address=(self._bind_address, 0))
  File "/usr/local/lib/python2.7/socket.py", line 575, in create_connection
    raise err
socket.error: [Errno 99] Cannot assign requested address

C:\src\iot310\week4>ls
'ls' is not recognized as an internal or external command,
operable program or batch file.

C:\src\iot310\week4>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\src\iot310\week4

04/21/2018  10:35 AM    <DIR>          .
04/21/2018  10:35 AM    <DIR>          ..
04/21/2018  10:33 AM    <DIR>          assets
04/19/2018  06:42 PM             1,119 centralVirtualDevice.py
04/21/2018  10:36 AM               939 Dockerfile
04/19/2018  08:33 PM            36,773 README.md
04/21/2018  10:36 AM                95 test.txt
               4 File(s)         38,926 bytes
               3 Dir(s)  280,319,082,496 bytes free

C:\src\iot310\week4>docker build -t central_virtual_device .
Sending build context to Docker daemon  252.9kB
Step 1/9 : FROM python:2.7
 ---> 2863c80c418c
Step 2/9 : RUN echo "I'm making a Docker container!"
 ---> Using cache
 ---> e54ad6a5cdbe
Step 3/9 : RUN apt-get update
 ---> Using cache
 ---> b5fdd915ebe8
Step 4/9 : RUN pip install paho-mqtt
 ---> Using cache
 ---> 0d4a8da253a2
Step 5/9 : RUN python --version
 ---> Using cache
 ---> f279cd482061
Step 6/9 : COPY centralVirtualDevice.py /centralVirtualDevice.py
 ---> Using cache
 ---> 599075040ee3
Step 7/9 : COPY test.txt /test.txt
 ---> Using cache
 ---> 4e2513c09139
Step 8/9 : WORKDIR /
 ---> Using cache
 ---> 1a9d13070876
Step 9/9 : CMD ["python", "-u", "centralVirtualDevice.py"]
 ---> Using cache
 ---> 5ffce9dc913e
Successfully built 5ffce9dc913e
Successfully tagged central_virtual_device:latest
SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.

C:\src\iot310\week4>docker run -it central_virtual_device
Sending local time : Sat Apr 21 18:03:12 2018
Sending local time : Sat Apr 21 18:03:14 2018
Sending local time : Sat Apr 21 18:03:16 2018
Sending local time : Sat Apr 21 18:03:18 2018
Sending local time : Sat Apr 21 18:03:20 2018
Sending local time : Sat Apr 21 18:03:22 2018
Sending local time : Sat Apr 21 18:03:24 2018
Sending local time : Sat Apr 21 18:03:26 2018
End of MQTT Messages

C:\src\iot310\week4>notepad AddCustomFileEvidence.txt

C:\src\iot310\week4>mkdir Evidence

C:\src\iot310\week4>mv AddCustomFileEvidence.txt Evidence
'mv' is not recognized as an internal or external command,
operable program or batch file.

C:\src\iot310\week4>move AddCustomFileEvidence.txt Evidence
        1 file(s) moved.

C:\src\iot310\week4> docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username (jerobart):
Password:
Login Succeeded

C:\src\iot310\week4>docker tag central_virtual_device richardjortega/central_virtual_device

C:\src\iot310\week4>docker tag central_virtual_device jerobart/central_virtual_device

C:\src\iot310\week4>docker push jerobart/central_virtual_device
The push refers to repository [docker.io/jerobart/central_virtual_device]
cf39acf1c988: Pushed
38e3901d2749: Layer already exists
acc11c8ec3bd: Layer already exists
8397be320c65: Layer already exists
ca82a2274c57: Layer already exists
de2fbb43bd2a: Layer already exists
4e32c2de91a6: Layer already exists
6e1b48dc2ccc: Layer already exists
ff57bdb79ac8: Layer already exists
6e5e20cbf4a7: Layer already exists
86985c679800: Layer already exists
8fad67424c4e: Layer already exists
latest: digest: sha256:341f6942a5b670c9c3dc4dc4da266b52f83f24a7bf4baafcdd991e8e7c9c8d83 size: 2846

C:\src\iot310\week4>cd Evidence

C:\src\iot310\week4\Evidence>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\src\iot310\week4\Evidence

04/21/2018  11:04 AM    <DIR>          .
04/21/2018  11:04 AM    <DIR>          ..
04/21/2018  11:03 AM             1,629 AddCustomFileEvidence.txt
               1 File(s)          1,629 bytes
               2 Dir(s)  280,327,163,904 bytes free

C:\src\iot310\week4\Evidence>notepad LinkToMyDockerRepoImage.txt

C:\src\iot310\week4\Evidence>cd ..

C:\src\iot310\week4>cd ..

C:\src\iot310>cd ..

C:\src>git clone https://github.com/Azure-Samples/docker-django-webapp-linux.git --config core.autocrlf=input
Cloning into 'docker-django-webapp-linux'...
remote: Counting objects: 225, done.
remote: Total 225 (delta 0), reused 0 (delta 0), pack-reused 225
Receiving objects: 100% (225/225), 1.22 MiB | 7.62 MiB/s, done.
Resolving deltas: 100% (55/55), done.

C:\src>cd docker-django-webapp-linux

C:\src\docker-django-webapp-linux>xcopy /E . c:\src\iot310\week4\docker-django-webapp-linux\
.\.gitignore
.\db.sqlite3
.\Dockerfile
.\init.sh
.\LICENSE
.\manage.py
.\readme.html
.\README.md
.\requirements.txt
.\sshd_config
.\app\forms.py
.\app\models.py
.\app\tests.py
.\app\views.py
.\app\__init__.py
.\app\migrations\__init__.py
.\app\static\app\content\bootstrap.css
.\app\static\app\content\bootstrap.min.css
.\app\static\app\content\site.css
.\app\static\app\fonts\glyphicons-halflings-regular.eot
.\app\static\app\fonts\glyphicons-halflings-regular.svg
.\app\static\app\fonts\glyphicons-halflings-regular.ttf
.\app\static\app\fonts\glyphicons-halflings-regular.woff
.\app\static\app\img\container.png
.\app\static\app\img\github.png
.\app\static\app\img\linux-web-app.png
.\app\static\app\img\overview.png
.\app\static\app\img\STB13_Ken_06.png
.\app\static\app\scripts\bootstrap.js
.\app\static\app\scripts\bootstrap.min.js
.\app\static\app\scripts\jquery-1.10.2.intellisense.js
.\app\static\app\scripts\jquery-1.10.2.js
.\app\static\app\scripts\jquery-1.10.2.min.js
.\app\static\app\scripts\jquery-1.10.2.min.map
.\app\static\app\scripts\jquery.validate-vsdoc.js
.\app\static\app\scripts\jquery.validate.js
.\app\static\app\scripts\jquery.validate.min.js
.\app\static\app\scripts\jquery.validate.unobtrusive.js
.\app\static\app\scripts\jquery.validate.unobtrusive.min.js
.\app\static\app\scripts\modernizr-2.6.2.js
.\app\static\app\scripts\respond.js
.\app\static\app\scripts\respond.min.js
.\app\static\app\scripts\_references.js
.\app\templates\app\index.html
.\app\templates\app\layout.html
.\djangoapp\settings.py
.\djangoapp\urls.py
.\djangoapp\wsgi.py
.\djangoapp\__init__.py
.\static\admin\css\base.css
.\static\admin\css\changelists.css
.\static\admin\css\dashboard.css
.\static\admin\css\fonts.css
.\static\admin\css\forms.css
.\static\admin\css\login.css
.\static\admin\css\rtl.css
.\static\admin\css\widgets.css
.\static\admin\fonts\LICENSE.txt
.\static\admin\fonts\README.txt
.\static\admin\fonts\Roboto-Bold-webfont.woff
.\static\admin\fonts\Roboto-Light-webfont.woff
.\static\admin\fonts\Roboto-Regular-webfont.woff
.\static\admin\img\calendar-icons.svg
.\static\admin\img\icon-addlink.svg
.\static\admin\img\icon-alert.svg
.\static\admin\img\icon-calendar.svg
.\static\admin\img\icon-changelink.svg
.\static\admin\img\icon-clock.svg
.\static\admin\img\icon-deletelink.svg
.\static\admin\img\icon-no.svg
.\static\admin\img\icon-unknown-alt.svg
.\static\admin\img\icon-unknown.svg
.\static\admin\img\icon-yes.svg
.\static\admin\img\inline-delete.svg
.\static\admin\img\LICENSE
.\static\admin\img\README.txt
.\static\admin\img\search.svg
.\static\admin\img\selector-icons.svg
.\static\admin\img\sorting-icons.svg
.\static\admin\img\tooltag-add.svg
.\static\admin\img\tooltag-arrowright.svg
.\static\admin\img\gis\move_vertex_off.svg
.\static\admin\img\gis\move_vertex_on.svg
.\static\admin\js\actions.js
.\static\admin\js\actions.min.js
.\static\admin\js\calendar.js
.\static\admin\js\cancel.js
.\static\admin\js\change_form.js
.\static\admin\js\collapse.js
.\static\admin\js\collapse.min.js
.\static\admin\js\core.js
.\static\admin\js\inlines.js
.\static\admin\js\inlines.min.js
.\static\admin\js\jquery.init.js
.\static\admin\js\popup_response.js
.\static\admin\js\prepopulate.js
.\static\admin\js\prepopulate.min.js
.\static\admin\js\prepopulate_init.js
.\static\admin\js\SelectBox.js
.\static\admin\js\SelectFilter2.js
.\static\admin\js\timeparse.js
.\static\admin\js\urlify.js
.\static\admin\js\admin\DateTimeShortcuts.js
.\static\admin\js\admin\RelatedObjectLookups.js
.\static\admin\js\vendor\jquery\jquery.js
.\static\admin\js\vendor\jquery\jquery.min.js
.\static\admin\js\vendor\jquery\LICENSE-JQUERY.txt
.\static\admin\js\vendor\xregexp\LICENSE-XREGEXP.txt
.\static\admin\js\vendor\xregexp\xregexp.js
.\static\admin\js\vendor\xregexp\xregexp.min.js
.\static\app\content\bootstrap.css
.\static\app\content\bootstrap.min.css
.\static\app\content\site.css
.\static\app\fonts\glyphicons-halflings-regular.eot
.\static\app\fonts\glyphicons-halflings-regular.svg
.\static\app\fonts\glyphicons-halflings-regular.ttf
.\static\app\fonts\glyphicons-halflings-regular.woff
.\static\app\img\container.png
.\static\app\img\github.png
.\static\app\img\linux-web-app.png
.\static\app\img\overview.png
.\static\app\scripts\bootstrap.js
.\static\app\scripts\bootstrap.min.js
.\static\app\scripts\jquery-1.10.2.intellisense.js
.\static\app\scripts\jquery-1.10.2.js
.\static\app\scripts\jquery-1.10.2.min.js
.\static\app\scripts\jquery-1.10.2.min.map
.\static\app\scripts\jquery.validate-vsdoc.js
.\static\app\scripts\jquery.validate.js
.\static\app\scripts\jquery.validate.min.js
.\static\app\scripts\jquery.validate.unobtrusive.js
.\static\app\scripts\jquery.validate.unobtrusive.min.js
.\static\app\scripts\modernizr-2.6.2.js
.\static\app\scripts\respond.js
.\static\app\scripts\respond.min.js
.\static\app\scripts\_references.js
136 File(s) copied

C:\src\docker-django-webapp-linux>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\src\docker-django-webapp-linux

04/21/2018  11:19 AM    <DIR>          .
04/21/2018  11:19 AM    <DIR>          ..
04/21/2018  11:19 AM             3,879 .gitignore
04/21/2018  11:19 AM    <DIR>          app
04/21/2018  11:19 AM            40,960 db.sqlite3
04/21/2018  11:19 AM    <DIR>          djangoapp
04/21/2018  11:19 AM               573 Dockerfile
04/21/2018  11:19 AM               109 init.sh
04/21/2018  11:19 AM             1,162 LICENSE
04/21/2018  11:19 AM               330 manage.py
04/21/2018  11:19 AM            28,392 readme.html
04/21/2018  11:19 AM             1,288 README.md
04/21/2018  11:19 AM                17 requirements.txt
04/21/2018  11:19 AM               453 sshd_config
04/21/2018  11:19 AM    <DIR>          static
              10 File(s)         77,163 bytes
               5 Dir(s)  280,271,241,216 bytes free

C:\src\docker-django-webapp-linux>cd \iot310\week4
The system cannot find the path specified.

C:\src\docker-django-webapp-linux>cd \src\iot310\week4

C:\src\iot310\week4>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\src\iot310\week4

04/21/2018  11:20 AM    <DIR>          .
04/21/2018  11:20 AM    <DIR>          ..
04/21/2018  10:33 AM    <DIR>          assets
04/19/2018  06:42 PM             1,119 centralVirtualDevice.py
04/21/2018  11:20 AM    <DIR>          docker-django-webapp-linux
04/21/2018  10:36 AM               939 Dockerfile
04/21/2018  11:07 AM    <DIR>          Evidence
04/19/2018  08:33 PM            36,773 README.md
04/21/2018  10:36 AM                95 test.txt
               4 File(s)         38,926 bytes
               5 Dir(s)  280,271,241,216 bytes free

C:\src\iot310\week4>cd di

C:\src\iot310\week4>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\src\iot310\week4

04/21/2018  11:20 AM    <DIR>          .
04/21/2018  11:20 AM    <DIR>          ..
04/21/2018  10:33 AM    <DIR>          assets
04/19/2018  06:42 PM             1,119 centralVirtualDevice.py
04/21/2018  11:20 AM    <DIR>          docker-django-webapp-linux
04/21/2018  10:36 AM               939 Dockerfile
04/21/2018  11:07 AM    <DIR>          Evidence
04/19/2018  08:33 PM            36,773 README.md
04/21/2018  10:36 AM                95 test.txt
               4 File(s)         38,926 bytes
               5 Dir(s)  280,271,241,216 bytes free

C:\src\iot310\week4>cd docker-django-webapp-linux

C:\src\iot310\week4\docker-django-webapp-linux>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\src\iot310\week4\docker-django-webapp-linux

04/21/2018  11:20 AM    <DIR>          .
04/21/2018  11:20 AM    <DIR>          ..
04/21/2018  11:19 AM             3,879 .gitignore
04/21/2018  11:20 AM    <DIR>          app
04/21/2018  11:19 AM            40,960 db.sqlite3
04/21/2018  11:20 AM    <DIR>          djangoapp
04/21/2018  11:19 AM               573 Dockerfile
04/21/2018  11:19 AM               109 init.sh
04/21/2018  11:19 AM             1,162 LICENSE
04/21/2018  11:19 AM               330 manage.py
04/21/2018  11:19 AM            28,392 readme.html
04/21/2018  11:19 AM             1,288 README.md
04/21/2018  11:19 AM                17 requirements.txt
04/21/2018  11:19 AM               453 sshd_config
04/21/2018  11:20 AM    <DIR>          static
              10 File(s)         77,163 bytes
               5 Dir(s)  280,271,192,064 bytes free

C:\src\iot310\week4\docker-django-webapp-linux>notepad Dockerfile

C:\src\iot310\week4\docker-django-webapp-linux>code .

C:\src\iot310\week4\docker-django-webapp-linux>dco

C:\src\iot310\week4\docker-django-webapp-linux>dir
 Volume in drive C is Windows
 Volume Serial Number is D0A1-58FF

 Directory of C:\src\iot310\week4\docker-django-webapp-linux

04/21/2018  11:20 AM    <DIR>          .
04/21/2018  11:20 AM    <DIR>          ..
04/21/2018  11:19 AM             3,879 .gitignore
04/21/2018  11:20 AM    <DIR>          app
04/21/2018  11:19 AM            40,960 db.sqlite3
04/21/2018  11:20 AM    <DIR>          djangoapp
04/21/2018  11:19 AM               573 Dockerfile
04/21/2018  11:19 AM               109 init.sh
04/21/2018  11:19 AM             1,162 LICENSE
04/21/2018  11:19 AM               330 manage.py
04/21/2018  11:19 AM            28,392 readme.html
04/21/2018  11:19 AM             1,288 README.md
04/21/2018  11:19 AM                17 requirements.txt
04/21/2018  11:19 AM               453 sshd_config
04/21/2018  11:20 AM    <DIR>          static
              10 File(s)         77,163 bytes
               5 Dir(s)  280,213,139,456 bytes free

C:\src\iot310\week4\docker-django-webapp-linux>docker build --tag jerobart/mydockerimage:v1.0.0 .
Sending build context to Docker daemon  4.212MB
Step 1/13 : FROM python:3.4
3.4: Pulling from library/python
f2b6b4884fc8: Already exists
4fb899b4df21: Already exists
74eaa8be7221: Already exists
2d6e98fe4040: Already exists
414666f7554d: Already exists
ad13e6696563: Pull complete
db302446208d: Pull complete
a6fd13d06818: Pull complete
Digest: sha256:0d7f19ba2d8efce83b009c57ec3be9299de093b7aec783d5b3dde3db28eae55e
Status: Downloaded newer image for python:3.4
 ---> 22cbab63509e
Step 2/13 : RUN mkdir /code
 ---> Running in a13627562592
Removing intermediate container a13627562592
 ---> aee834309dcb
Step 3/13 : WORKDIR /code
Removing intermediate container e123008054b4
 ---> 0eb8d672da80
Step 4/13 : ADD requirements.txt /code/
 ---> 8fdf63f83db7
Step 5/13 : RUN pip install -r requirements.txt
 ---> Running in 9d26f1eb379a
Collecting django<2,>=1.9 (from -r requirements.txt (line 1))
  Downloading https://files.pythonhosted.org/packages/92/56/1f30c1e6a58b0c97c492461148edcbece2c6e43dcc3529695165744349ee/Django-1.11.12-py2.py3-none-any.whl (6.9MB)
Collecting pytz (from django<2,>=1.9->-r requirements.txt (line 1))
  Downloading https://files.pythonhosted.org/packages/dc/83/15f7833b70d3e067ca91467ca245bae0f6fe56ddc7451aa0dc5606b120f2/pytz-2018.4-py2.py3-none-any.whl (510kB)
Installing collected packages: pytz, django
Successfully installed django-1.11.12 pytz-2018.4
Removing intermediate container 9d26f1eb379a
 ---> 1aabdcdaf091
Step 6/13 : ADD . /code/
 ---> b7a52bc436d8
Step 7/13 : ENV SSH_PASSWD "root:Docker!"
 ---> Running in 842ee01dd049
Removing intermediate container 842ee01dd049
 ---> 8fcdb0d8ebc4
Step 8/13 : RUN apt-get update         && apt-get install -y --no-install-recommends dialog         && apt-get update   && apt-get install -y --no-install-recommends openssh-server    && echo "$SSH_PASSWD" | chpasswd
 ---> Running in 2c50be1bcb8b
Get:1 http://security.debian.org jessie/updates InRelease [94.4 kB]
Ign http://deb.debian.org jessie InRelease
Get:2 http://deb.debian.org jessie-updates InRelease [145 kB]
Get:3 http://deb.debian.org jessie Release.gpg [2434 B]
Get:4 http://deb.debian.org jessie Release [148 kB]
Get:5 http://security.debian.org jessie/updates/main amd64 Packages [646 kB]
Get:6 http://deb.debian.org jessie-updates/main amd64 Packages [23.1 kB]
Get:7 http://deb.debian.org jessie/main amd64 Packages [9064 kB]
Fetched 10.1 MB in 6s (1550 kB/s)
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following NEW packages will be installed:
  dialog
0 upgraded, 1 newly installed, 0 to remove and 15 not upgraded.
Need to get 255 kB of archives.
After this operation, 1416 kB of additional disk space will be used.
Get:1 http://deb.debian.org/debian/ jessie/main dialog amd64 1.2-20140911-1 [255 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 255 kB in 1s (226 kB/s)
Selecting previously unselected package dialog.
(Reading database ... 21636 files and directories currently installed.)
Preparing to unpack .../dialog_1.2-20140911-1_amd64.deb ...
Unpacking dialog (1.2-20140911-1) ...
Setting up dialog (1.2-20140911-1) ...
Hit http://security.debian.org jessie/updates InRelease
Ign http://deb.debian.org jessie InRelease
Hit http://deb.debian.org jessie-updates InRelease
Get:1 http://security.debian.org jessie/updates/main amd64 Packages [646 kB]
Hit http://deb.debian.org jessie Release.gpg
Hit http://deb.debian.org jessie Release
Get:2 http://deb.debian.org jessie-updates/main amd64 Packages [23.1 kB]
Get:3 http://deb.debian.org jessie/main amd64 Packages [9064 kB]
Fetched 9733 kB in 6s (1615 kB/s)
Reading package lists...
Reading package lists...
Building dependency tree...
Reading state information...
The following extra packages will be installed:
  init-system-helpers libwrap0 openssh-sftp-server
Suggested packages:
  ssh-askpass rssh molly-guard ufw monkeysphere
Recommended packages:
  tcpd xauth ncurses-term
The following NEW packages will be installed:
  init-system-helpers libwrap0 openssh-server openssh-sftp-server
0 upgraded, 4 newly installed, 0 to remove and 15 not upgraded.
Need to get 442 kB of archives.
After this operation, 1293 kB of additional disk space will be used.
Get:1 http://deb.debian.org/debian/ jessie/main libwrap0 amd64 7.6.q-25 [58.5 kB]
Get:2 http://deb.debian.org/debian/ jessie/main init-system-helpers all 1.22 [14.0 kB]
Get:3 http://deb.debian.org/debian/ jessie/main openssh-sftp-server amd64 1:6.7p1-5+deb8u4 [37.9 kB]
Get:4 http://deb.debian.org/debian/ jessie/main openssh-server amd64 1:6.7p1-5+deb8u4 [331 kB]
debconf: delaying package configuration, since apt-utils is not installed
Fetched 442 kB in 1s (346 kB/s)
Selecting previously unselected package libwrap0:amd64.
(Reading database ... 21792 files and directories currently installed.)
Preparing to unpack .../libwrap0_7.6.q-25_amd64.deb ...
Unpacking libwrap0:amd64 (7.6.q-25) ...
Selecting previously unselected package init-system-helpers.
Preparing to unpack .../init-system-helpers_1.22_all.deb ...
Unpacking init-system-helpers (1.22) ...
Selecting previously unselected package openssh-sftp-server.
Preparing to unpack .../openssh-sftp-server_1%3a6.7p1-5+deb8u4_amd64.deb ...
Unpacking openssh-sftp-server (1:6.7p1-5+deb8u4) ...
Selecting previously unselected package openssh-server.
Preparing to unpack .../openssh-server_1%3a6.7p1-5+deb8u4_amd64.deb ...
Unpacking openssh-server (1:6.7p1-5+deb8u4) ...
Processing triggers for systemd (215-17+deb8u7) ...
Setting up libwrap0:amd64 (7.6.q-25) ...
Setting up init-system-helpers (1.22) ...
Setting up openssh-sftp-server (1:6.7p1-5+deb8u4) ...
Setting up openssh-server (1:6.7p1-5+deb8u4) ...
debconf: unable to initialize frontend: Dialog
debconf: (TERM is not set, so the dialog frontend is not usable.)
debconf: falling back to frontend: Readline
Creating SSH2 RSA key; this may take some time ...
2048 83:39:f0:fe:66:ea:a9:4f:73:8f:80:6e:f9:33:3e:9b /etc/ssh/ssh_host_rsa_key.pub (RSA)
Creating SSH2 DSA key; this may take some time ...
1024 39:a2:f5:62:74:6e:06:59:0d:c1:0e:ec:46:f5:b6:ce /etc/ssh/ssh_host_dsa_key.pub (DSA)
Creating SSH2 ECDSA key; this may take some time ...
256 3d:1d:f0:4f:98:84:aa:56:4b:73:c5:32:16:8f:da:31 /etc/ssh/ssh_host_ecdsa_key.pub (ECDSA)
Creating SSH2 ED25519 key; this may take some time ...
256 4f:19:51:20:fe:fb:6e:53:44:4d:cd:b3:ca:9e:97:2d /etc/ssh/ssh_host_ed25519_key.pub (ED25519)
invoke-rc.d: policy-rc.d denied execution of start.
Processing triggers for libc-bin (2.19-18+deb8u10) ...
Processing triggers for systemd (215-17+deb8u7) ...
Removing intermediate container 2c50be1bcb8b
 ---> 9f7a06750e0e
Step 9/13 : COPY sshd_config /etc/ssh/
 ---> 5e56565d979f
Step 10/13 : COPY init.sh /usr/local/bin/
 ---> 5e898e772c22
Step 11/13 : RUN chmod u+x /usr/local/bin/init.sh
 ---> Running in 8f05b72c8b6e
Removing intermediate container 8f05b72c8b6e
 ---> ddf5d39ccbd6
Step 12/13 : EXPOSE 8000 2222
 ---> Running in d30b004211cd
Removing intermediate container d30b004211cd
 ---> 46b08107bf3b
Step 13/13 : ENTRYPOINT ["init.sh"]
 ---> Running in 20fd883707f3
Removing intermediate container 20fd883707f3
 ---> 902eb65622be
Successfully built 902eb65622be
Successfully tagged jerobart/mydockerimage:v1.0.0
SECURITY WARNING: You are building a Docker image from Windows against a non-Windows Docker host. All files and directories added to build context will have '-rwxr-xr-x' permissions. It is recommended to double check and reset permissions for sensitive files and directories.

C:\src\iot310\week4\docker-django-webapp-linux>docker run -p 2222:8000 jerobart/mydockerimage:v1.0.0
Starting SSH ...
Starting OpenBSD Secure Shell server: sshd.
[21/Apr/2018 18:27:23] "GET / HTTP/1.1" 200 7947
[21/Apr/2018 18:27:23] "GET /static/app/content/bootstrap.min.css HTTP/1.1" 200 97949
[21/Apr/2018 18:27:23] "GET /static/app/content/site.css HTTP/1.1" 200 1362
[21/Apr/2018 18:27:23] "GET /static/app/img/overview.png HTTP/1.1" 200 1995
[21/Apr/2018 18:27:23] "GET /static/app/scripts/modernizr-2.6.2.js HTTP/1.1" 200 51458
[21/Apr/2018 18:27:23] "GET /static/app/img/container.png HTTP/1.1" 200 2955
[21/Apr/2018 18:27:23] "GET /static/app/img/linux-web-app.png HTTP/1.1" 200 11826
[21/Apr/2018 18:27:23] "GET /static/app/scripts/bootstrap.js HTTP/1.1" 200 59250
[21/Apr/2018 18:27:23] "GET /static/app/scripts/respond.js HTTP/1.1" 200 10318
[21/Apr/2018 18:27:23] "GET /static/app/scripts/jquery-1.10.2.js HTTP/1.1" 200 273990
[21/Apr/2018 18:27:23] "GET /static/app/img/github.png HTTP/1.1" 200 9411
[21/Apr/2018 18:27:23] "GET /static/app/img/STB13_Ken_06.png HTTP/1.1" 200 401177

C:\src\iot310\week4\docker-django-webapp-linux>
C:\src\iot310\week4\docker-django-webapp-linux>
C:\src\iot310\week4\docker-django-webapp-linux>
C:\src\iot310\week4\docker-django-webapp-linux>
C:\src\iot310\week4\docker-django-webapp-linux>
C:\src\iot310\week4\docker-django-webapp-linux>docker ps
CONTAINER ID        IMAGE                           COMMAND             CREATED             STATUS              PORTS                              NAMES
10f427f942b9        jerobart/mydockerimage:v1.0.0   "init.sh"           5 minutes ago       Up 5 minutes        2222/tcp, 0.0.0.0:2222->8000/tcp   reverent_wozniak

C:\src\iot310\week4\docker-django-webapp-linux>docker stop 10f427f942b9
10f427f942b9

C:\src\iot310\week4\docker-django-webapp-linux>docker push jerobart/mydockerimage:v1.0.0
The push refers to repository [docker.io/jerobart/mydockerimage]
a6a8250be6fd: Pushed
58cf608ebda9: Pushed
b9722e3fce61: Pushed
979120a35434: Pushed
a6fe1fd5c0ab: Pushed
bc4c59ef02b3: Pushed
aded7b2c7a37: Pushed
f15bec270a9c: Mounted from library/python
ea57150ca622: Mounted from library/python
47954f4b8fc8: Mounted from library/python
6e1b48dc2ccc: Mounted from jerobart/central_virtual_device
ff57bdb79ac8: Mounted from jerobart/central_virtual_device
6e5e20cbf4a7: Mounted from jerobart/central_virtual_device
86985c679800: Mounted from jerobart/central_virtual_device
8fad67424c4e: Mounted from jerobart/central_virtual_device
v1.0.0: digest: sha256:961b1ceb2ca6ea50be02ae648ef46881582b6f79818a9e9f36c1cc9515152ab7 size: 3676

C:\src\iot310\week4\docker-django-webapp-linux>az group create --name myResourceGroup --location eastus
'az' is not recognized as an internal or external command,
operable program or batch file.

C:\src\iot310\week4\docker-django-webapp-linux>